
export function onValidateTextInput(event, id, validationType, maxLength, fieldMsg) {

  let letters, msg, value = event.target.value;
  if (fieldMsg && fieldMsg.VALIDATION) {
    for (var i = 0; i < fieldMsg.VALIDATION.length; i++) {
      if (fieldMsg.VALIDATION[i].dataType === "Text" && fieldMsg.VALIDATION[i].validationType === "Max Length" && fieldMsg.VALIDATION[i].usage === "Field") {
        var LenErrorMsg = fieldMsg.VALIDATION[i].message;
      }
      else if (fieldMsg.VALIDATION[i].dataType === "Text" && fieldMsg.VALIDATION[i].validationType === "Characters Only" && fieldMsg.VALIDATION[i].usage === "Field") {
        var CharOnlyMsg = fieldMsg.VALIDATION[i].message;
      }
      else if (fieldMsg.VALIDATION[i].dataType === "Text" && fieldMsg.VALIDATION[i].validationType === "Characters And Numbers" && fieldMsg.VALIDATION[i].usage === "Field") {
        var CharNumMsg = fieldMsg.VALIDATION[i].message;
      }
    }
  }
  if (validationType === "textOnly") {
    letters = /^[ A-Za-z]*$/;
    msg = CharOnlyMsg;
  } else if (validationType === "textAndNum") {
    letters = /^[ A-Za-z0-9]*$/;
    msg = CharNumMsg;
  }
  if (maxLength !== "" && (value.length > parseInt(maxLength, 10))) {
    return ({ value: "error", msg: LenErrorMsg, eventvalue: value });
  }
  else if ((!value.match(letters))) {
    return ({ value: "error", msg: msg, eventvalue: value });
  }
  else {
    return ({ value: "none", msg: "", eventvalue: value });
  }
};

export function onValidateNumberInput(event, intDigits, decimalDigits, fieldMsg) {
  let value = event.target.value;
  if (fieldMsg) {
    for (var i = 0; i < fieldMsg.VALIDATION.length; i++) {
      if (fieldMsg.VALIDATION[i].dataType === "Number" && fieldMsg.VALIDATION[i].validationType === "Limit" && fieldMsg.VALIDATION[i].usage === "Field") {
        var LenErrorMsg = fieldMsg.VALIDATION[i].message;
      }
    }
  }
  if (value.includes(".")) {
    var num = value.split(".");
    if (num[0].length > parseInt(intDigits) || num[1].length > parseInt(decimalDigits)) {
      // this.setState({ ...this.state, numberValueState: "Error",numberValueStateMsg: "Exceeds Length" });
      return ({ value: "error", msg: LenErrorMsg, eventvalue: value });
    } else {
      return ({ value: "none", msg: "", eventvalue: value });
    }
  } else {
    if (value.length > parseInt(intDigits)) {
      return ({ value: "error", msg: LenErrorMsg, eventvalue: value });
    } else {
      return ({ value: "none", msg: "", eventvalue: value });
    }
  }
};

export function onValidateTextArea(value, maxLength, fieldMsg) {


  if (fieldMsg && fieldMsg.VALIDATION) {
    for (var i = 0; i < fieldMsg.VALIDATION.length; i++) {
      if (fieldMsg.VALIDATION[i].dataType === "Long Text" && fieldMsg.VALIDATION[i].validationType === "Max Length" && fieldMsg.VALIDATION[i].usage === "Field") {
        var LenErrorMsg = fieldMsg.VALIDATION[i].message;
      }
    }
  }

  if (value.length > parseInt(maxLength)) {
    return ({ value: "error", msg: LenErrorMsg, eventvalue: value });
  } else {
    return ({ value: "none", msg: "", eventvalue: value });
  }
};

export function onValidateEmail(value, fieldMsg) {
  if (fieldMsg && fieldMsg.VALIDATION) {
    for (var i = 0; i < fieldMsg.VALIDATION.length; i++) {
      if (fieldMsg.VALIDATION[i].dataType === "Email" && fieldMsg.VALIDATION[i].validationType === "Match Regex" && fieldMsg.VALIDATION[i].usage === "Field") {
        var ErrorMsg = fieldMsg.VALIDATION[i].message;
      }
    }
  }

  // let reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  const reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (value.length && (!value.match(reg))) {
    return ({ value: "error", msg: ErrorMsg, eventvalue: value });
  } else {
    return ({ value: "none", msg: "", eventvalue: value });
  }
};

export function onValidateName(value, surname, fieldMsg) {
  let letters, msg;
  if (fieldMsg && fieldMsg.VALIDATION) {
    for (var i = 0; i < fieldMsg.VALIDATION.length; i++) {
      if (fieldMsg.VALIDATION[i].dataType === "Name" && fieldMsg.VALIDATION[i].validationType === "Alphabets Only" && fieldMsg.VALIDATION[i].usage === "Field") {
        var ErrorMsg = fieldMsg.VALIDATION[i].message;
      }
    }
  }

  letters = /^[ A-Za-z.]*$/;

  if ((!value.match(letters))) {
    return ({ value: "error", msg: ErrorMsg, eventvalue: surname + ' ' + value });
  } else {
    return ({ value: "none", msg: "", eventvalue: surname + ' ' + value });
  }
}

export function onValidateDate(controlType, e, v, format, fieldMsg) {
  if (fieldMsg && fieldMsg.VALIDATION) {
    for (var i = 0; i < fieldMsg.VALIDATION.length; i++) {
      if (fieldMsg.VALIDATION[i].dataType === "Date" && fieldMsg.VALIDATION[i].validationType === "Max" && fieldMsg.VALIDATION[i].usage === "Field") {
        var ErrorMsg = fieldMsg.VALIDATION[i].message;
      }
      if (fieldMsg.VALIDATION[i].dataType === "Date" && fieldMsg.VALIDATION[i].validationType === "Date Format" && fieldMsg.VALIDATION[i].usage === "Field") {
        var dateFormatMsg = fieldMsg.VALIDATION[i].message + " " + format;
      }
      if (fieldMsg.VALIDATION[i].dataType === "Time" && fieldMsg.VALIDATION[i].validationType === "Time Format" && fieldMsg.VALIDATION[i].usage === "Field") {
        var timeFormatMsg = fieldMsg.VALIDATION[i].message + " " + format;
      }
      if (fieldMsg.VALIDATION[i].dataType === "Date/Time" && fieldMsg.VALIDATION[i].validationType === "Date/Time Format" && fieldMsg.VALIDATION[i].usage === "Field") {
        var dateTimeFormatMsg = fieldMsg.VALIDATION[i].message + " " + format;
      }
    }
  }
  const msg = controlType === 'date' ? dateFormatMsg : controlType === 'time' ? timeFormatMsg : dateTimeFormatMsg;
  if (e == 'Invalid Date') {
    return ({ value: "error", msg: msg, eventvalue: v });
  }
  else {
    return ({ value: "none", msg: "", eventvalue: v });
  }
}

