import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Tab from '@material-ui/core/Tab';
import TabContext from '@material-ui/lab/TabContext';
import TabList from '@material-ui/lab/TabList';
import TabPanel from '@material-ui/lab/TabPanel';
import * as Icons from '@material-ui/icons';
import "./scss/form-builder.scss";
import ReactDOM from 'react-dom';
import { onValidateTextInput, onValidateNumberInput, onValidateTextArea, onValidateEmail, onValidateName, onValidateDate } from './validationFunc';

const drawerWidth = 240;
const appbarHeight = 3;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        width: `calc(100% - 3rem)`,
        backgroundColor: getComputedStyle(document.documentElement).getPropertyValue('--ternaryColor').trim(),
        color: getComputedStyle(document.documentElement).getPropertyValue('--primaryColor').trim(),
    },
    appBar: {
        zIndex: 'inherit',
        position: 'relative',
        height: '3rem',
        justifyContent: 'center',
        boxShadow: 'none',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },

    menuButton: {
        // marginRight: theme.spacing(1),
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        position: 'relative',
        zIndex: 'inherit',
    },

    drawerOpen: {
        width: drawerWidth,
        position: 'relative',
        zIndex: 'inherit',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        position: 'relative',
        zIndex: 'inherit',
        width: theme.spacing(7) + 1,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
    },

    content: {
        flexGrow: 1,
        width: 'calc(100% - 14rem)'

    },
    contentShift: {

    },
}));

export default function SideNavWithTabs(props) {

    const classes = useStyles();
    const [open, setOpen] = useState(true);

    const handleDrawerOpen = () => {
        setOpen(!open);
    };

    return (
        // <div style={{ height: 'calc(100% - 4.1rem)' }}>
        <React.Fragment>
            <AppBar position="fixed" className={clsx(classes.appBar)} >
                <Toolbar className="customAppBar">
                    <IconButton color="inherit" aria-label="open drawer" onClick={handleDrawerOpen} edge="start" className={clsx(classes.menuButton)} >
                        <MenuIcon />
                    </IconButton>
                    
                    <text className="customTabSideNav"> {props.formTitle}</text>
                </Toolbar>
            </AppBar>
            <div className={classes.root}>
                <CssBaseline />
               
                <Drawer
                    variant="permanent"
                    className={clsx(classes.drawer, {
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    })}
                    classes={{
                        paper: clsx({
                            [classes.drawerOpen]: open,
                            [classes.drawerClose]: !open,
                        }),
                    }}
                >
                    <List className="customTabSideNav" style={{ paddingTop: "0", paddingLeft:'0.5rem' }}>
                        {props.sideNavList.map((nav, index) => {
                            let Icon = Icons[nav.icon]
                            return (
                                <ListItem button key={nav.text} disabled={nav.disabled} hidden={!nav.visible} selected={nav.value === props.navValue} onClick={() => props.onNavItemClick(nav.value)}>
                                    {nav.icon ? <ListItemIcon><Icon /></ListItemIcon> : null}
                                    <ListItemText primary={nav.text} />
                                </ListItem>
                            )
                        })}
                    </List>
                </Drawer>
           
                <main
                    className={clsx(classes.content, {
                        [classes.contentShift]: open,
                    })}
                >
                    <div >
                        {props.previewData}
                    </div>
                </main>
        
            </div>
        </React.Fragment>
    );
}


