import React from "react";
import jsPDF from "jspdf";
import "jspdf-autotable";
import { CSVLink } from "react-csv";
import GridTable from "@nadavshaar/react-grid-table";
import TextField from '@material-ui/core/TextField';
//import FormElements from "../CwWfFormElements";
//import ComponentPropertyMapping from "../CwWfComponentPropertyMapping";
import { ResponsivePopover } from "@ui5/webcomponents-react/dist/ResponsivePopover";
import { Label } from "@ui5/webcomponents-react/dist/Label";
import { Button } from "@ui5/webcomponents-react/dist/Button";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import FilterList from "@material-ui/icons/FilterList";
import ReactExport from "react-export-excel";
import ID from "./UUID";
import { components } from "react-select";
import { is } from "date-fns/locale";
import { isEmptyObject } from "jquery";
const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class Table extends React.Component {
  constructor(props) {
    super(props);
    this.filterPopoverRef = React.createRef();
    this.inputField = React.createRef();
    this.operatorRef = React.createRef();
    this.valueRef = React.createRef();
    this.value2Ref = React.createRef();
    this.inputs = [];

  }

  state = {
    valueState: "None",
    valueStateMessage: "",
    attributes: [],
    rows: [],
    componentsList: {},
    tableComponent: {},
    // rows: this.props.defaultValue || [],
    editRows: [],
    deleteRows: [],
    addRows: [],
    selectedRows: [],
    filters: [
      {
        id: "cfe78e09-d139-4d64-ba6d-31f36cc3256c",
        key: "not eq",
        value: "Not Equal To",
        additionalValue: "NVARCHAR",
      },
      {
        id: "1b7e628f-2455-463e-b3bd-170113cbc84d",
        key: "eq",
        value: "Equal To",
        additionalValue: "NVARCHAR",
      },
      {
        id: "47e3e501-fed5-4ae9-8857-0d48633941b5",
        key: "not_in",
        value: "Not In",
        additionalValue: "NVARCHAR",
      },
      {
        id: "b3e4def5-e4a9-4790-a0f7-dfd24c77ac83",
        key: "starts with",
        value: "Starts With",
        additionalValue: "NVARCHAR",
      },
      {
        id: "cf2be281-fb34-441d-9f7e-4b0c5bfe6b28",
        key: "ends with",
        value: "Ends With",
        additionalValue: "NVARCHAR",
      },
      {
        id: "85831085-fcf8-4403-9f80-6ab4d2f939ff",
        key: "contains",
        value: "Contains",
        additionalValue: "NVARCHAR",
      },
      {
        id: "02a1de87-1613-49e8-ba2a-5a9a20541e26",
        key: "less than",
        value: "Less Than",
        additionalValue: "DECIMAL",
      },
      {
        id: "6babf4ea-0e0b-4285-ac41-df939b7ddb58",
        key: "less than eq",
        value: "Less Than Or Equal To",
        additionalValue: "DECIMAL",
      },
      {
        id: "51f943b6-7660-43b1-8ebd-ed9ad4ce91d4",
        key: "greater than",
        value: "Greater Than",
        additionalValue: "DECIMAL",
      },
      {
        id: "98d4d065-dc60-4c49-aaea-22ada940212e",
        key: "greater than eq",
        value: "Greater Than Or Equal To",
        additionalValue: "DECIMAL",
      },
      {
        id: "ee0894d0-6da2-4f0a-a82b-27ff4bb9a09d",
        key: "between",
        value: "Between",
        additionalValue: "DECIMAL",
      },
      {
        id: "2a92862b-8f86-40bc-b3d4-bd137236d00c",
        key: "not eq",
        value: "Not Equal To",
        additionalValue: "DECIMAL",
      },
      {
        id: "efdc7b1d-4592-4a11-adbf-b1b80dd9a8c9",
        key: "eq",
        value: "Equal To",
        additionalValue: "DECIMAL",
      },
      {
        id: "b83cfd3f-b496-4d7f-9721-04977585178d",
        key: "less than",
        value: "Less Than",
        additionalValue: "INTEGER",
      },
      {
        id: "a774dcc2-7416-4a40-ac71-dadb9d97b588",
        key: "not eq",
        value: "Not Equal To",
        additionalValue: "INTEGER",
      },
      {
        id: "ead32ad7-8f1a-4ff6-bcfd-023f956c357b",
        key: "eq",
        value: "Equal To",
        additionalValue: "INTEGER",
      },
      {
        id: "6744f59d-0c58-4375-9c04-5604ecadbf0e",
        key: "less than eq",
        value: "Less Than Or Equal To",
        additionalValue: "INTEGER",
      },
      {
        id: "20b834bc-ea0e-42fa-a76d-acc045021441",
        key: "greater than",
        value: "Greater Than",
        additionalValue: "INTEGER",
      },
      {
        id: "b6a6f015-6759-48e3-acf0-a00e466b282e",
        key: "greater than eq",
        value: "Greater Than Or Equal To",
        additionalValue: "INTEGER",
      },
      {
        id: "4e4ec529-75f5-438c-ad58-cbfdfcbb8cfb",
        key: "between",
        value: "Between",
        additionalValue: "INTEGER",
      },
      {
        id: "be14e303-b4d3-406e-a0dc-b1e6f5f50bdd",
        key: "eq",
        value: "Equal To",
        additionalValue: "DATE",
      },
      {
        id: "97e5d8d4-6107-41ae-8ae5-9fca7f727ae7",
        key: "between",
        value: "Between",
        additionalValue: "DATE",
      },
      {
        id: "a6eb757a-8419-4011-a123-589d174d4736",
        key: "eq",
        value: "Equal To",
        additionalValue: "BOOLEAN",
      },
      {
        id: "85544d87-3fb5-4a2a-907a-7b41727c6112",
        key: "not eq",
        value: "Not Equal To",
        additionalValue: "BOOLEAN",
      },
      {
        id: "tye78e09-d139-4d64-ba6d-31f36cc3256c",
        key: "eq",
        value: "Equal To",
        additionalValue: "TIMESTAMP",
      },
      {
        id: "cui78e09-d139-4d64-ba6d-31f36cc3256c",
        key: "between",
        value: "Between",
        additionalValue: "TIMESTAMP",
      },
    ],
    columnFilters: [],
    selectedFilters: [],
    currentColumn: "",
    editRowId: "",
    filterObj: [],
    modeAdd: false,
  };

  // componentDidUpdate(prevProps) {
  //   if ( this.props.defaultValue && prevProps.defaultValue !== this.props.defaultValue ) {
  //     this.setState({ rows: this.props.defaultValue.rows, addRows:this.props.defaultValue.addRows  });
  //   }
  // }

  componentDidMount() {
    debugger;
    let busFormId = this.props.busFormId
    // Promise.all([
    //   fetch("/workrules/v1/lapi?lookupName=Operators").then((res) => res.json()),
    //   fetch("/workforms/busformjsonbyid/get/" + busFormId).then((res) => res.json())
    // ])
    //   .then(([lookUpResonse, businessResponse]) => {
    //     const lookupresponse = lookUpResonse.data, business = businessResponse;
    //     var decoded = JSON.parse(atob(business.jsonData));
    //     const componentsList = decoded.components;
    //     this.setState({
    //       ...this.state,
    //       filters: lookupresponse.valueList,
    //       rows: this.getRows(),
    //       componentsList: componentsList
    //     }, () => this.setTableId());
    //   })

    fetch("/workrules/v1/lapi?lookupName=Operators")
      .then((response) => response.json())
      .then((data) =>
        this.setState({
          ...this.state,
          filters: data.data.valueList,
          rows: this.getRows(),
        })
      );

    let filterObj = this.props.tableComponent.WOAttributesList.map((attribute) => ({
      field: attribute.technicalAttributeName,
      operator: "",
      value: "",
      value2: "",
      isActive: false,
    }));
    // this.setState({ rows: this.getRows(), filterObj: filterObj });
    // let rows = [], addRows = [], editRows = [], deleteRows = [];
    // if (this.props.defaultValue) {
    //   // rows = this.props.defaultValue.addRows;
    //   rows = this.props.defaultValue.rows;
    //   addRows = this.props.defaultValue.addRows;
    //   editRows = this.props.defaultValue.editRows;
    //   deleteRows = this.props.defaultValue.deleteRows;
    // }
    // this.setState({ rows: rows, addRows: addRows, editRows: editRows, deleteRows: deleteRows, filterObj: filterObj })
  }
  setTableId = () => {
    for (var idx in this.state.componentsList) {
      if (this.state.componentsList[idx].componentName === "Table" && this.state.componentsList[idx].id === this.props.tableId)
        this.setState({ ...this.state, tableComponent: this.state.componentsList[idx], attributes: this.state.componentsList[idx].WOAttributesList });
    }
  }
  tableError = () => {
    this.setState({ valueState: "Error" });
  };

  getInputElement(attribute, value, data, column, onChange) {
    // let item;
    // //debugger;
    //if(attribute.dataElementDto.dateType === "String" && !attribute.dataElementDto.lookUpName) {
     // <TextField className="customInput2" type="text" variant="outlined" size="small" disabled={false} placeholder="" style={{ width: "100%" }} />
     // <Tooltip title=""><TextField className="customInput2"  type="text" variant ="outlined" size="small"  style={{ width: "100%" }} placeholder="` + item.properties.placeholder.propertyValue + `" style={​​​​​​​​{​​​​​​​​width:"100%"}​​​​​​​​}​​​​​​​​ maxLength={attribute.dataElementDto.length}​​​​​​​​​​​​​​​​/></Tooltip>;
    //}
    // if (
    //   attribute.dataElementDto.dataType === "String" &&
    //   !attribute.dataElementDto.lookUpName
    // ) {
    //   item = ComponentPropertyMapping.find(
    //     (item) => item.componentName === "TextInput"
    //   );
    //   item = JSON.parse(JSON.stringify(item));
    //   item.properties.maxLength.propertyValue = attribute.dataElementDto.length;
    //   item.properties.textInputValidaion.propertyValue = "freeText";
    // } else if (
    //   attribute.dataElementDto.dataType === "String" &&
    //   attribute.dataElementDto.lookUpName
    // ) {
    //   item = ComponentPropertyMapping.find(
    //     (item) => item.componentName === "Dropdown"
    //   );
    //   item = JSON.parse(JSON.stringify(item));
    //   item.properties.lookUpName.propertyValue =
    //     attribute.dataElementDto.lookUpName;
    // } else if (attribute.dataElementDto.dataType === "Date") {
    //   item = ComponentPropertyMapping.find(
    //     (item) => item.componentName === "DatePickers"
    //   );
    //   item = JSON.parse(JSON.stringify(item));
    // } else if (
    //   attribute.dataElementDto.dataType === "Integer" ||
    //   attribute.dataElementDto.dataType === "BigDecimal"
    // ) {
    //   item = ComponentPropertyMapping.find(
    //     (item) => item.componentName === "NumberInput"
    //   );
    //   item = JSON.parse(JSON.stringify(item));
    //   item.properties.intDigits.propertyValue = attribute.dataElementDto.length;
    //   item.properties.decimalDigits.propertyValue =
    //     attribute.dataElementDto.noOfDecimals;
    // }
    // item.inputField = React.createRef();
    // item.field_name = "table_" + ID.uuid();
    // item.properties.required.propertyValue = attribute.mandatory;
    // item.inTable = true;
    // item.value = value;
    // item.properties.disabled.propertyValue = this.state.modeAdd
    //   ? false
    //   : attribute.primaryKey;

    // this.inputs.push(item);

    //const Input = FormElements[item.componentName];
    return (
      <TextField className="customInput2" type="text" variant ="outlined" size="small"  style={{ width: "100%" }} style={{width:"100%"}}/>
      //<TextField className="customInput2" type="text" variant="outlined" size="small" disabled={this.state.modeAdd? false : attribute.primaryKey} placeholder="" style={{ width: "100%" }} maxLength={attribute.dataElementDto.length}​​​​​​​​​​​​​​​​ value={value} required={attribute.mandatory}/>
    );
  }

  onHeaderCellClicked = (e, column) => {
    // console.log(column);
    e.preventDefault();
    let col = this.props.tableComponent.WOAttributesList.find((ele) => ele.id === column.id);
    let colDataType = col.dataElementDto.dataType;
    // console.log(colDataType);
    let filterType;
    if (colDataType === "String") filterType = "NVARCHAR";
    else if (colDataType === "BigDecimal") filterType = "DECIMAL";
    else if (colDataType === "Integer") filterType = "INTEGER";
    else if (colDataType === "Date") filterType = "DATE";
    else if (colDataType === "Boolean") filterType = "BOOLEAN";
    let colFilters = this.state.filters.filter(
      (filter) => filter.additionalValue === filterType
    );
    // console.log(colFilters);
    this.setState({ columnFilters: colFilters, currentColumn: column.field });

    this.filterPopoverRef.current.open(e.target);
  };

  formatDate = (format, value) => {
    if (!value) {
      return value;
    }
    let yyyy, MM, dd;
    value = value.toString();
    if (value.includes("-")) {
      var arr = value.split("-");
      yyyy = arr[0];
      MM = arr[1];
      dd = arr[2];
    } else {
      const val = parseInt(value);
      value = isNaN(val) ? value : val;
      const date = new Date(value);
      yyyy = date.getFullYear();
      MM = date.getMonth() + 1;
      dd = date.getDate();
    }

    let formattedDate = "";
    if (format === "dd/MM/yyyy") {
      formattedDate = dd + "/" + MM + "/" + yyyy;
    } else if (format === "MM/dd/yyyy") {
      formattedDate = MM + "/" + dd + "/" + yyyy;
    } else if (format === "dd-MM-yyyy") {
      formattedDate = dd + "-" + MM + "-" + yyyy;
    } else if (format === "MM-dd-yyyy") {
      formattedDate = MM + "-" + dd + "-" + yyyy;
    }
    return formattedDate;
  };

  getColumns = () => {
    this.inputs = [];
    let saveDisabled = false;
    // for(let i=0; i<this.inputs.length; i++) {
    //   let ref = this.inputs[i].inputField;
    //   let valueState = ref.current.state.valueState;
    //   if(valueState === "Error") {
    //     saveDisabled = true;
    //   }
    // }
    // debugger;
    
      let cols = this.props.tableComponent.WOAttributesList.map((attribute) => ({
        id: attribute.id,
        resizable: false,
        width: "224px",
        field: attribute.technicalAttributeName,
        label: attribute.label,
        getValue: ({ value, column }) => {
          if (attribute.dataElementDto.dataType === "Date" && value) {
            // return new Date(value).toLocaleDateString();
            return this.formatDate("dd/MM/yyyy", value);
            // return value;
          } else {
            return value;
          }
        },
        //   setValue: ({value, data, setRow, column}) => {
        //     // value: '35',
        //     // data: { ..., columnField: { fieldToUpdate: '27' }}
        //     let rowClone = { ...data };
        //     debugger;
        //     if(attribute.dataElementDto.dataType === "Date") {
        //       rowClone[column.field] = 11;
        //       setRow(rowClone);
        //       }
        // },
        sortable:
          this.props.tableComponent.properties.sortable.propertyValue
            .toString()
            .toLowerCase() === "true",
        headerCellRenderer: ({ tableManager, column }) => {
          let filterObj = [...this.state.filterObj];
          let isActive = false;
          //flag=false;
          filterObj.map((ele) => {
            if (ele.field === column.field) {
              isActive = ele.isActive;
            }
          });
          //flag =  ? true: false;

          return (
            // <div style={{width: "100%"}} >
            <div style={{ display: "flex", width: "100%" }}>
              <label
                className="customLabel"
                style={{ textAlign: "left", padding: "12px 0" }}
                wrap={true}
              >
                {attribute.label}
              </label>
              {this.props.tableComponent.properties.filterable.propertyValue
                .toString()
                .toLowerCase() === "true" ? (
                <IconButton
                  aria-label="Filter"
                  style={
                    isActive
                      ? { color: "green", marginLeft: "auto" }
                      : { marginLeft: "auto" }
                  }
                  onClick={(e) => this.onHeaderCellClicked(e, column)}
                >
                  <FilterList />
                </IconButton>
              ) : null}
            </div>
          );
        },
        editorCellRenderer: ({
          tableManager,
          value,
          data,
          column,
          colIndex,
          rowIndex,
          onChange,
        }) => this.getInputElement(attribute, value, data, column, onChange),
      }));
      //if (this.props.data.properties.editable.propertyValue.toString().toLowerCase() === "true") {
      let edit = {
        id: "action-buttons-column",
        width: "max-content",
        pinned: true,
        sortable: false,
        resizable: false,
        cellRenderer: ({
          tableManager,
          value,
          data,
          column,
          colIndex,
          rowIndex,
        }) => {
          let edit = null,
            deleteicon = null;
          if (
            this.props.tableComponent.properties.editable.propertyValue
              .toString()
              .toLowerCase() === "true"
          ) {
            edit = (
              <IconButton
                aria-label="Edit"
                style={{ color: "green" }}
                onClick={(e) => this.setState({ editRowId: data.id })}
              >
                <EditIcon />
              </IconButton>
            );
          }
          if (
            this.props.tableComponent.properties.deleteRecord.propertyValue
              .toString()
              .toLowerCase() === "true"
          ) {
            deleteicon = (
              <IconButton
                aria-label="Delete"
                style={{ color: "red" }}
                onClick={() => this.onDeleteRecord(data)}
              >
                <DeleteIcon />
              </IconButton>
            );
          }
          return (
            <div>
              {edit} {deleteicon}
            </div>
          );
        },
        editorCellRenderer: ({
          tableManager,
          value,
          data,
          column,
          colIndex,
          rowIndex,
          onChange,
        }) => {
          // for(let i=0; i<this.inputs.length; i++) {
          //   let ref = this.inputs[i].inputField;
          //   let valueState = ref.current && ref.current.state.valueState;
          //   if(valueState === "Error") {
          //     saveDisabled = true;
          //   }
          // }
          return (
            <div style={{ display: "inline-flex" }}>
              <IconButton
                aria-label="Save"
                style={{ color: "green" }}
                //  disabled={saveDisabled}
                onClick={(e) => {
                  for (let i = 0; i < this.inputs.length; i++) {
                    let ref = this.inputs[i].inputField;
                    let valueState = ref.current && ref.current.state.valueState;
                    if (valueState === "Error") {
                      return;
                      //saveDisabled = true;
                    }
                  }
                  if (this.state.valueState === "Error") {
                    window.alert("error");
                  }

                  let rowsClone = [...this.state.rows];
                  let editRows = [...this.state.editRows];

                  let addRows = [...this.state.addRows];
                  let flag = true;
                  addRows = addRows.map((row) => {
                    if (row.id === data.id) {
                      flag = false;
                      return data;
                    } else return row;
                  });
                  if (flag) {
                    let editFlag = true;
                    editRows = editRows.map((row) => {
                      if (row.id === data.id) {
                        editFlag = false;
                        return data;
                      } else return row;
                    });
                    if (editFlag) {
                      editRows.push(data);
                    }
                  }
                  let updatedRowIndex = rowsClone.findIndex(
                    (r) => r.id === data.id
                  );
                  rowsClone[updatedRowIndex] = data;
                  this.setState({
                    rows: rowsClone,
                    editRowId: "",
                    editRows: editRows,
                    addRows: addRows,
                    modeAdd: false,
                  });
                }}
              >
                <CheckIcon />
              </IconButton>
              <IconButton
                aria-label="Cancel"
                style={{ color: "red" }}
                onClick={(e) => {
                  if (
                    this.props.tableComponent.properties.deleteRecord.propertyValue
                      .toString()
                      .toLowerCase() === "false"
                  ) {
                    //this.onDeleteRecord(data)
                    let rowsClone = [...this.state.rows];
                    let addRows = [...this.state.addRows];
                    addRows.map((row) => {
                      if (row.id === data.id) {
                        let values = Object.values(row);
                        values.shift();
                        if (values.every((o) => o === "")) {
                          this.onDeleteRecord(data, "temp");
                        }
                      }
                    });
                  }
                  this.setState({ editRowId: "", modeAdd: false });
                }}
              >
                <ClearIcon />
              </IconButton>
            </div>
          );
        },
      };

      cols.push(edit);
      //}
      // if ( this.props.data.properties.deleteRecord.propertyValue.toString().toLowerCase() === "true") {
      //   cols.unshift({ id: "checkbox" });
      // }
      return cols;
  };

  getRows = () => {
    return [];
  };

  onAddRecord = (tableManager, e) => {
    let rows = [...this.state.rows];
    let addRows = [...this.state.addRows];
    let id = ID.uuid();
    let row = { id: id };
    const attributes = this.props.tableComponent.WOAttributesList;
    for (const attribute of attributes) {
      row[attribute.technicalAttributeName] = "";
    }
    rows.push(row);
    addRows.push(row);
    this.setState({
      rows: rows,
      editRowId: id,
      addRows: addRows,
      modeAdd: true,
    });
  };

  // rowSelected = (e, tableManager, rowIndex, data) => {
  //   console.log(e.target.checked, rowIndex, tableManager);
  //   let selectedRowsClone = [...this.state.selectedRows];
  //   if (e.target.checked) {
  //     selectedRowsClone.push(data);
  //     //this.setState({...this.state, selectedRows:selectedRowsClone})
  //     this.state.selectedRows = selectedRowsClone;
  //   } else {
  //     const index = selectedRowsClone.indexOf(data);
  //     selectedRowsClone.splice(index, 1);
  //     this.state.selectedRows = selectedRowsClone;
  //   }
  //   console.log(this.state.selectedRows);
  // };

  onDeleteRecord = (data, type) => {
    // let deleteRows = this.state.deleteRows;
    // if (type !== "temp") {
    //   deleteRows.push(data);
    // }

    // let remainingRows,
    // remainingAddRows = [...this.state.addRows];
    // //let ids = tableManager.rowSelectionApi.selectedRowsIds;
    // let ids = [data.id];
    // var currentSet = new Set(ids);
    // remainingRows = this.state.rows.filter((x) => !currentSet.has(x.id));
    // if (type === "temp") {
    //   remainingAddRows = this.state.addRows.filter(
    //     (x) => !currentSet.has(x.id)
    //   );
    // }
    // this.setState({
    //   rows: remainingRows,
    //   selectedRows: [],
    //   deleteRows: deleteRows,
    //   addRows: remainingAddRows,
    // });

    const deleteItemId = data.id;
    let editRows = this.state.editRows, addRows = this.state.addRows, deleteRows = [...this.state.deleteRows];
    const rows = this.state.rows.filter(x => x.id !== deleteItemId);
    const checkAddRows = this.state.addRows.find(x => x.id === deleteItemId);
    const checkEditRows = this.state.editRows.find(x => x.id === deleteItemId);
    if (checkAddRows) {
      addRows = addRows.filter(x => x.id !== deleteItemId)
    } else if (checkEditRows) {
      editRows = editRows.filter(x => x.id !== deleteItemId)
      deleteRows.push(checkEditRows)
    } else {
      deleteRows.push(data)
    }

    this.setState({ rows: rows, addRows: addRows, editRows: editRows, deleteRows: deleteRows })

  };

  onExportTableAsPDF = () => {
    // console.log("export table");
    const dataSetName = this.props.tableComponent.WOdataSetName;
    const unit = "pt";
    const size = "A4"; // Use A1, A2, A3 or A4
    const orientation = "portrait"; // portrait or landscape

    const marginLeft = 40;
    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(15);

    const title = dataSetName;
    const cols = this.props.tableComponent.WOAttributesList;
    const header = cols.map((col) => col.label);
    const headers = [[...header]];
    const data = this.state.rows.map((elt) =>
      Object.keys(elt)
        .slice(1)
        .map((key) => elt[key])
    );
    //debugger;
    let content = {
      startY: 50,
      head: headers,
      body: data,
    };

    doc.text(title, marginLeft, 40);
    doc.autoTable(content);
    doc.save(dataSetName + ".pdf");
  };

  onExportTable = () => {
    const columns = Object.keys(this.getRows()[0]);
    const filterColsByKey = columns.filter((c) => c !== "id");
    return filterColsByKey;
  };

  CustomHeader = ({ tableManager }) => {
      const { searchApi, columnsVisibilityApi, columnsApi } = tableManager;

      const { searchText, setSearchText } = searchApi;
      const { toggleColumnVisibility } = columnsVisibilityApi;
      const { columns } = columnsApi;
      return (
        <div>
          <div style={{ display: "flex", marginTop: 10 }}>
            {this.props.tableComponent.properties.addRecord.propertyValue
              .toString()
              .toLowerCase() === "true" && (
                <button
                  className=""
                  type="button"
                  onClick={(e) => this.onAddRecord(tableManager, e)}
                >
                  Add Record
                </button>
              )}
            {/* {this.props.data.properties.deleteRecord.propertyValue.toString().toLowerCase() === "true" && (
            <button type="button" onClick={() => this.onDeleteRecord(tableManager)}>
              Delete Records
            </button>
          )} */}
            {this.props.tableComponent.properties.exportTable.propertyValue
              .toString()
              .toLowerCase() === "true" && (
                // <button type="button" onClick={this.onExportTable}>
                //   Export Table
                // </button>
                <React.Fragment>
                  <button type="button">
                    <CSVLink
                      style={{ color: "black", textDecoration: "none" }}
                      // data={this.getRows()}
                      data={this.state.rows}
                      headers={Object.keys(
                        this.state.rows.length ? this.state.rows[0] : []
                      ).filter((c) => c !== "id")}
                      filename={this.props.tableComponent.WOdataSetName + ".csv"}
                    >
                      Download as CSV
                    </CSVLink>
                  </button>
                  <ExcelFile
                    filename={this.props.tableComponent.WOdataSetName}
                    element={
                      <button style={{ height: "100%" }}>Download as Excel</button>
                    }
                  >
                    <ExcelSheet data={this.state.rows} name="Sheet1">
                      {Object.keys(this.state.rows.length ? this.state.rows[0] : [])
                        .filter((c) => c !== "id")
                        .map((col) => {
                          return <ExcelColumn label={col} value={col} />;
                        })}
                    </ExcelSheet>
                  </ExcelFile>
                </React.Fragment>
              )}
            <div style={{ marginLeft: "auto" }}>
              <label
                className="customLabel"
                htmlFor="my-search"
                style={{ fontWeight: 500, marginRight: 10 }}
              >
                Search for:
              </label>
              <input
                className="customInput2"
                name="my-search"
                type="search"
                value={searchText}
                onChange={(e) => setSearchText(e.target.value)}
                style={{ width: 200 }}
              />
            </div>
          </div>
        </div>
      );
  };

  onAddFilter = () => {
    this.filterPopoverRef.current.close();
    let filter = {
      field: this.state.currentColumn,
      operator: this.operatorRef.current.value,
      value: this.valueRef.current.value,
      value2: this.value2Ref.current.value,
      isActive: true,
    };
    let filterObj = [...this.state.filterObj];
    filterObj.map((ele) => {
      if (ele.field === this.state.currentColumn) {
        ele.value = this.valueRef.current.value;
        ele.value2 = this.value2Ref.current.value;
        ele.operator = this.operatorRef.current.value;
        ele.isActive = true;
      }
    });
    let selectedFilters = [...this.state.selectedFilters];
    selectedFilters.push(filter);
    this.setState({ selectedFilters: selectedFilters, filterObj: filterObj });
    // console.log(filter)
  };

  filterRows = (rows) => {
      if (
        this.props.tableComponent.properties.filterable.propertyValue
          .toString()
          .toLowerCase() === "true"
      ) {
        let tempRows = [...rows];
        let selectedFilters = this.state.selectedFilters;
        for (let i = 0; i < selectedFilters.length; i++) {
          let field = selectedFilters[i].field;
          let operator = selectedFilters[i].operator;
          let value = selectedFilters[i].value;
          let value2 = selectedFilters[i].value2;
          tempRows = tempRows.filter((row) => {
            if (operator === "eq") {
              return row[field] === value;
            } else if (operator === "not eq") {
              return row[field] !== value;
            } else if (operator === "less than") {
              return row[field] < value;
            } else if (operator === "less than eq") {
              return row[field] <= value;
            } else if (operator === "greater than") {
              return row[field] > value;
            } else if (operator === "greater than eq") {
              return row[field] >= value;
            } else if (operator === "starts with") {
              return row[field].startsWith(value);
            } else if (operator === "ends with") {
              return row[field].endsWith(value);
            } else if (operator === "contains") {
              return row[field].includes(value);
            } else if (operator === "not_in") {
              return !row[field].includes(value);
            } else if (operator === "between") {
              return row[field] > value && row[field] < value2;
            }
          });
        }
        return tempRows;
      } else return rows;
  };

  onRemoveFilter = () => {
    this.filterPopoverRef.current.close();
    let filter = {
      field: this.state.currentColumn,
      operator: this.operatorRef.current.value,
      value: this.valueRef.current.value,
      value2: this.value2Ref.current.value,
      isActive: false,
    };
    let filterObj = [...this.state.filterObj];
    filterObj.map((ele) => {
      if (ele.field === this.state.currentColumn) {
        ele.value = "";
        ele.value2 = "";
        ele.operator = "";
        ele.isActive = false;
      }
    });
    let selectedFilters = [...this.state.selectedFilters];
    selectedFilters = selectedFilters.filter(
      (item) => item.field !== filter.field
    );
    this.setState({ selectedFilters: selectedFilters, filterObj: filterObj });
  };

  inputChangeHandler = (e) => {
    let filterObj = [...this.state.filterObj];
    filterObj.map((ele) => {
      if (ele.field === this.state.currentColumn) {
        ele.value = e.target.value;
        //ele.operator = this.operatorRef.current.value
      }
    });
    this.setState({ filterObj: filterObj });
  };

  input2ChangeHandler = (e) => {
    let filterObj = [...this.state.filterObj];
    filterObj.map((ele) => {
      if (ele.field === this.state.currentColumn) {
        ele.value2 = e.target.value;
        //ele.operator = this.operatorRef.current.value
      }
    });
    this.setState({ filterObj: filterObj });
  };

  operatorChangeHandler = (e) => {
    let filterObj = [...this.state.filterObj];
    filterObj.map((ele) => {
      if (ele.field === this.state.currentColumn) {
        //ele.value= e.target.value;
        ele.operator = e.target.value;
      }
    });
    this.setState({ filterObj: filterObj });
  };

  render() {
    let col = this.getColumns();
    let baseClasses = "SortableItem rfb-item";

    let filterObj = [...this.state.filterObj];
    let value = "", value2 = "", operator = "";
    filterObj.map((ele) => {
      if (ele.field === this.state.currentColumn) {
        value = ele.value;
        value2 = ele.value2;
        operator = ele.operator;
      }
    });

    let hideInput2 = true;
    if (operator === "between") {
      hideInput2 = false;
    }

    return (
      <div>
        <div className="form-group">
          <GridTable
            ref={this.inputField}
            editRowId={this.state.editRowId}
            key={Math.random()}
            columns={col}
            rows={this.filterRows(this.state.rows)}
            components={{ Header: this.CustomHeader }}
          />
        </div>
        <ResponsivePopover ref={this.filterPopoverRef} placementType="Bottom">
          <div style={{ margin: "1rem 1rem 0.5rem 1rem" }}>
            <div style={{ display: "flex" }}>
              <label> Operator </label>
              <select
                style={{ width: "60%", marginLeft: "auto", marginRight: "0" }}
                ref={this.operatorRef}
                value={operator}
                onChange={this.operatorChangeHandler}
              >
                {this.state.columnFilters.map((ele) => (
                  <option key={ele.id} value={ele.key}>
                    {ele.value}
                  </option>
                ))}
              </select>
            </div>
            <div style={{ display: "flex", marginTop: "0.5rem" }}>
              <label> Value</label>
              <input
                style={{ width: "60%", marginLeft: "auto", marginRight: "0" }}
                ref={this.valueRef}
                value={value}
                onChange={this.inputChangeHandler}
              />
            </div>
            <div style={{ display: "flex", marginTop: "0.5rem" }}>
              <label hidden={hideInput2}> Value 2</label>
              <input
                style={{ width: "60%", marginLeft: "auto", marginRight: "0" }}
                hidden={hideInput2}
                ref={this.value2Ref}
                value={value2}
                onChange={this.input2ChangeHandler}
              />
            </div>
          </div>
          <div slot="footer" style={{ marginLeft: "auto", marginRight: "0" }}>
            <button type="button" onClick={this.onAddFilter}>
              Add
            </button>
            <button type="button" onClick={this.onRemoveFilter}>
              Remove
            </button>
          </div>
        </ResponsivePopover>
      </div>
    );
  }
}

export default Table;

// class Table extends React.Component {
//   constructor(props) {
//     super(props);
//   }
//   componentDidMount = () => {
//     console.log(this.props);
//   }
//   render(){
//     console.log(this.props);
//     return(<div></div>);
//   }
// }

// export default Table;