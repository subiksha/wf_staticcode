const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/workforms',
    createProxyMiddleware({
      target: 'https://workforms.cfapps.eu10.hana.ondemand.com',
      changeOrigin: true,
    })
  );
  app.use(
    '/WorkObjectsJavaServices',
    createProxyMiddleware({
      target: 'https://modellingapi.cfapps.eu10.hana.ondemand.com',
      changeOrigin: true,
      pathRewrite: {'^/WorkObjectsJavaServices' : ''}
    })
  );
  app.use(
    '/WorkObjectsDataJavaServices',
    createProxyMiddleware({
      target: 'https://hrapps.cfapps.eu10.hana.ondemand.com',
      changeOrigin: true,
      pathRewrite: {'^/WorkObjectsDataJavaServices' : ''}
    })
  );
  app.use(
    '/CW_Worktext',
    createProxyMiddleware({
      target: 'https://cwworktext.cfapps.eu10.hana.ondemand.com',
      changeOrigin: true,
      pathRewrite: {'^/CW_Worktext' : ''}
    })
  );
  app.use(
    '/workrules',
    createProxyMiddleware({
      target: 'https://incture-technologies-hrapps-cw-price-management-cw-work648755a0.cfapps.eu10.hana.ondemand.com',
      changeOrigin: true,
      pathRewrite: {'^/workrules' : '/rest'}
    })
  );
};