import React from "react";
import CwWfmodelledForm from "./CwWfmodelledForm";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
export default class App extends React.Component {

  render() {
    return (
      <React.Fragment>
        <Router>
          <Switch>
            <Route exact path="/" component={CwWfmodelledForm}>   
            </Route>
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}